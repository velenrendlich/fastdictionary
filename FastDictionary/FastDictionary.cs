﻿using System;
using System.Runtime.CompilerServices;

namespace FastDictionary
{
	// Insert can insert an item in a deleted slot, but search can’t stop at a deleted slot.
	public class FastDictionary<TKey, TValue> where TKey : IEquatable<TKey>
	{
		public struct KeyValuePair
		{
			public TKey Key;
			public TValue Value;
		}

		public int Count { get; private set; }

		int[] Hashes;
		KeyValuePair[] KeyValuePairs;
		readonly float RehashLoadFactor;

		public int Capacity
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => Hashes.Length;
		}

		const float DefaultRehashLoadFactor = 0.8f;
		const int EmptyHashCode = -1;
		const int DeletedHashCode = -2;

		// TODO: Inlining for small methods, no inlining for rare paths
		// TODO: Remove 'in' for now. That's for later
		// TODO: Add KeyValuePair again
		// TODO: Simplify GetValue logic again
		public TValue this[TKey key]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => GetValueForKey(key);

			// TODO: Inline this
			set => SetValue(key, value);
		}

		public FastDictionary(int initialCapacity = 16, float rehashLoadFactor = DefaultRehashLoadFactor)
		{
			if(initialCapacity <= 0)
				throw new Exception("Capacity should be greater than zero.");
			if(rehashLoadFactor is <= 0 or >= 1)
				throw new Exception("Rehash load factor should be between 0 and 1.");

			RehashLoadFactor = rehashLoadFactor;

			KeyValuePairs = new KeyValuePair[initialCapacity];
			Hashes = new int[initialCapacity];

			for(int i = 0; i < Hashes.Length; i++)
			{
				Hashes[i] = EmptyHashCode;
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static int CalculateHash(TKey key)
		{
			return key.GetHashCode() & 0x7FFFFFFF;
		}

		void AddEntryNoChecks(TKey key, TValue value)
		{
			var hash = CalculateHash(key);
			var length = KeyValuePairs.Length;
			var index = hash % length;

			for(var i = index; i < length; i++)
			{
				switch(Hashes[i])
				{
					case EmptyHashCode:
					{
						Hashes[i] = hash;
						KeyValuePairs[i] = new KeyValuePair
						{
							Key = key,
							Value = value
						};
						return;
					}
					case DeletedHashCode:
						continue;
					default:
					{
						if(hash == Hashes[i] && key.Equals(KeyValuePairs[i].Key))
						{
							throw new Exception($"Dictionary already contains Key {key}.");
						}

						continue;
					}
				}
			}

			for(int i = 0; i < index; i++)
			{
				switch(Hashes[i])
				{
					case EmptyHashCode:
					{
						Hashes[i] = hash;
						KeyValuePairs[i] = new KeyValuePair
						{
							Key = key,
							Value = value
						};
						return;
					}
					case DeletedHashCode:
						continue;
					default:
					{
						if(hash == Hashes[i] && key.Equals(KeyValuePairs[i].Key))
						{
							throw new Exception($"Dictionary already contains Key {key}.");
						}

						continue;
					}
				}
			}

			throw new Exception(
				$"Couldn't insert item to any index. Key: {key}, Value: {value}, Capacity: {Capacity}.");
		}

		public void Add(TKey key, TValue value)
		{
			var newCount = Count + 1;
			var newLoadFactor = (float)newCount / Capacity;
			if(newLoadFactor >= RehashLoadFactor)
			{
				Rehash();
			}

			// TODO: Rename to AddEntryNoLoadChecks
			AddEntryNoChecks(key, value);
			Count++;
		}

		void SetValue(TKey key, TValue value)
		{
			var capacity = Capacity;
			var hash = CalculateHash(key);
			var index = hash % capacity;

			for(int i = index; i < capacity; i++)
			{
				switch(Hashes[i])
				{
					case EmptyHashCode:
						throw GetNotInDictionaryException(key);
					case DeletedHashCode:
						continue;
					default:
					{
						if(hash == Hashes[i] && key.Equals(KeyValuePairs[i].Key))
						{
							KeyValuePairs[i].Value = value;
							return;
						}

						continue;
					}
				}
			}

			for(int i = 0; i < index; i++)
			{
				switch(Hashes[i])
				{
					case EmptyHashCode:
						throw GetNotInDictionaryException(key);
					case DeletedHashCode:
						continue;
					default:
					{
						if(hash == Hashes[i] && key.Equals(KeyValuePairs[i].Key))
						{
							KeyValuePairs[i].Value = value;
							return;
						}

						continue;
					}
				}
			}

			throw GetNotInDictionaryException(key);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		Exception GetNotInDictionaryException(TKey key)
		{
			return new Exception($"The key '{key}' does not exist in the Dictionary.");
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		TValue GetValueForKey(TKey key)
		{
			if(TryGetValue(key, out var value))
				return value;

			throw GetNotInDictionaryException(key);
		}

		// TODO: When you have the time, try 2 for loop vs. while loop performance. Don't know why is this so slow atm.
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool TryGetValue(TKey key, out TValue value)
		{
			var capacity = Capacity;
			var keyHash = CalculateHash(key);
			var lookupIndex = keyHash % capacity;
			var lookupCount = 0;
			var hashAtLookupIndex = Hashes[lookupIndex];

			// lookupCount logic is correct right?
			while(hashAtLookupIndex != EmptyHashCode && lookupCount++ < capacity)
			{
				if(keyHash == hashAtLookupIndex)
				{
					if(key.Equals(KeyValuePairs[lookupIndex].Key))
					{
						value = KeyValuePairs[lookupIndex].Value;
						return true;
					}
				}

				lookupIndex = (lookupIndex + 1) % capacity;
				hashAtLookupIndex = Hashes[lookupIndex];
			}

			value = default;
			return false;
		}

		public bool Remove(TKey key)
		{
			return Remove(key, out _);
		}

		public bool Remove(TKey key, out TValue value)
		{
			var capacity = Capacity;
			var hash = CalculateHash(key);
			var index = hash % capacity;
			value = default;

			for(int i = index; i < capacity; i++)
			{
				switch(Hashes[i])
				{
					case EmptyHashCode:
						return false;
					case DeletedHashCode:
						continue;
					default:
					{
						if(hash == Hashes[i] && key.Equals(KeyValuePairs[i].Key))
						{
							Hashes[i] = DeletedHashCode;
							value = KeyValuePairs[i].Value;
							Count--;
							return true;
						}

						continue;
					}
				}
			}

			for(int i = 0; i < index; i++)
			{
				switch(Hashes[i])
				{
					case EmptyHashCode:
						return false;
					case DeletedHashCode:
						continue;
					default:
					{
						if(hash == Hashes[i] && key.Equals(KeyValuePairs[i].Key))
						{
							Hashes[i] = DeletedHashCode;
							value = KeyValuePairs[i].Value;
							Count--;
							return true;
						}

						continue;
					}
				}
			}

			return false;
		}

		public bool ContainsKey(TKey key)
		{
			var capacity = Capacity;
			var keyHash = CalculateHash(key);
			var lookupIndex = keyHash % capacity;
			var lookupCount = 0;
			var hashAtLookupIndex = Hashes[lookupIndex];

			while(hashAtLookupIndex != EmptyHashCode && lookupCount++ < capacity)
			{
				if(hashAtLookupIndex == keyHash)
				{
					if(key.Equals(KeyValuePairs[lookupIndex].Key))
						return true;
				}
				
				lookupIndex = (lookupCount + 1) % capacity;
				hashAtLookupIndex = Hashes[lookupIndex];
			}

			return false;
		}

		// TODO: Release KeyValuePairs too, release managed resources
		public void Clear()
		{
			for(int i = 0; i < Hashes.Length; i++)
			{
				Hashes[i] = EmptyHashCode;
			}

			Count = 0;
		}

		void Rehash()
		{
			var newCapacity = Capacity * 2;
			var oldKeyValuePairs = KeyValuePairs;
			var oldHashes = Hashes;

			Hashes = new int[newCapacity];
			KeyValuePairs = new KeyValuePair[newCapacity];

			for(int i = 0; i < Hashes.Length; i++)
			{
				Hashes[i] = EmptyHashCode;
			}

			for(int i = 0; i < oldHashes.Length; i++)
			{
				// There's non-empty entry, insert to new table
				if(oldHashes[i] >= 0)
				{
					AddEntryNoChecks(oldKeyValuePairs[i].Key, oldKeyValuePairs[i].Value);
				}
			}
		}
	}
}